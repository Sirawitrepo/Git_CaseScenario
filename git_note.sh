# Define git variable
# https://stackoverflow.com/questions/26620312/git-installing-git-in-path-with-github-client-for-windows


# Cloning to current path
git clone https://github.com/Veerarit/Create_Mock_Up_Data.git 
cd Create_Mock_Up_Data 

# Cloning to specific folder
git clone https://github.com/Veerarit/Create_Mock_Up_Data.git {folder_name}

# Cloning from specific branch
git clone --branch dev https://github.com/Veerarit/Create_Mock_Up_Data.git test_github

# Create another branch from a current branch
git checkout -b {new_branch_name}

# Add changes from this file to staging area
git add {file_name}

# Add changes from every file in this directory to staging area
git add {.}

# You're sure this version of code
git commit {file_name}
git commit {file_name} -m "{your_messages}"

# Commit all files that have been added
git commit -a

git commit -a -m "{your_messages}"

# Send committed changes to remote repo
git push

# undo a commit
git reset {file_name}

# Check current status of changes/state in the directory
git status 

# Check logging
git log


# Making a Pull Request #
# https://docs.github.com/en/pull-requests/collaborating-with-pull-requests/incorporating-changes-from-a-pull-request/merging-a-pull-request
# https://docs.github.com/en/pull-requests/collaborating-with-pull-requests/incorporating-changes-from-a-pull-request/about-pull-request-merges
# All commits from this branch will be added to the base branch via a merge commit
Create a merge commit 

# The 3 commits from this branch will be combined into one commit in the base branch
Squash and merge 

# The 3 commits from this branch will be rebased and added to the base branch
Rebase and merge 
